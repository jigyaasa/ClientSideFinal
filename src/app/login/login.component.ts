import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { RouterModule, Router } from '@angular/router';
import { LoginService } from './login.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  protected emailid;
  protected formdata;
  protected errorMessage: string = "";
 
  constructor(private router: Router, private loginService:  LoginService) { }

  ngOnInit() {
    this.formdata = new FormGroup({
        cid: new FormControl(""),
        username: new FormControl(""),
        passwd: new FormControl("")
    });
  }

  onClickSubmit(data) {

    console.log("Inside submit button " + JSON.stringify(data))
    
    if ((data.cid === "") && (data.username === "") && (data.passwd === "")) {
      setTimeout(() => {
        this.errorMessage = ""
      }, 3000);
      this.errorMessage = "Please Enter values"
     } else {
      let body =  "cid=" + data.cid + "&uname=" + data.username + "&password=" + data.passwd;
      console.log("Inside submit button payload " + body)
      this.loginService.checkLoginDeatils(body)
        .subscribe(
          (response) => {
            console.log("Response :- " + response['_body'])
            if(response['_body'] === "Login Successful"){
              this.router.navigate(['updateInfo']); 
            } else if(response['_body'] === "Login Failed"){
              setTimeout(() => {
                this.errorMessage = ""
              }, 3000);
              this.errorMessage = "Login Failed"
            }
          }
        )

      
    }
  }

  onSignUp(){
    console.log("Inside sign up page")
    this.router.navigate(['register']);
  }

}
