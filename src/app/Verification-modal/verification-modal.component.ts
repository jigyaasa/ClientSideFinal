import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { RouterModule, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VerificationService } from "./verification-modal.service";

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './verification-modal.component.html',
  styleUrls: ['./verification-modal.component.css']
})

export class VerificationModal implements OnInit {
  protected formdata;

  @Input() name;
  
  constructor(private router: Router, public activeModal: NgbActiveModal, private verificationService: VerificationService) {}
  
  ngOnInit() {
    console.log("name + " +this.name)
    this.formdata = new FormGroup({
      secret: new FormControl(""),
    });
  }

  onClickSubmit(data) {
    console.log(this.name + "Inside submit button " + JSON.stringify(data))
    let body =  "cid=" + this.name + "&otp=" + data.secret;
    this.verificationService.verifyOTPCreate(body)
        .subscribe((response) => {
          console.log("Response :- " + response['_body'])
          if( response['_body'] == "OTP verified") {
            this.router.navigate(['updateInfo']);
            this.activeModal.dismiss('Cross click');
          }
        })
  }

  onSignIn(){
    this.router.navigate(['login']);
    this.activeModal.dismiss('Cross click');
  }

}