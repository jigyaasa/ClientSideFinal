import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Injectable()
export class RegisterService {

    constructor(private http: Http) { }
    
    registerService(payload) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("http://ec2-18-218-131-46.us-east-2.compute.amazonaws.com/register", payload, { headers: headers })
            .map(res => res)
    }

}