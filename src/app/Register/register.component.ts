import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VerificationModal } from './../Verification-modal/verification-modal.component'
import { RegisterService } from './register.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  protected formdata;  
  protected errorMessage: string = "";
  protected successMessage: string = "";

  constructor(private router: Router, private modalService: NgbModal, private registerService: RegisterService) { }

  ngOnInit() {
    this.formdata = new FormGroup({
      fname: new FormControl(""),
      mname: new FormControl(""),
      lname: new FormControl(""),
      cid: new FormControl(""),
      uname: new FormControl(""),
      passwd: new FormControl(""),
      contactNumber: new FormControl(""),
      email: new FormControl(""),
      address: new FormControl(""),
      upload: new FormControl("")
    });
  }

  onClickSubmit(data) {
    console.log("Inside submit button " + JSON.stringify(data));
    if(data.uname === "" && data.passwd === "")  {
      setTimeout(() => {
        this.errorMessage = ""
      }, 3000);
      this.errorMessage = "Username and Password should not be empty"
    } else {
      
      const payload = {
      "cid" : data.cid,
      "fname":data.fname,
      "lname":data.lname,
      "uname":data.uname,
      "password":data.passwd,
      "email":data.email,
      "contactnumber":'+' + data.contactNumber,
      "address":data.address,
      "thumb":data.upload
      }
      
      this.registerService.registerService(payload)
        .subscribe((response) => {
          console.log("Response :- " + response['_body'])
          if (response['_body'] == "Enter Your OTP") {
            const modalRef = this.modalService.open(VerificationModal);
            modalRef.componentInstance.name = data.cid;
          }

        })

    }
  }

  onSignIn(){
    this.router.navigate(['login']);
  }

}
