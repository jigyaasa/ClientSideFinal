import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router'; 
import { FormGroup, FormControl } from '@angular/forms';
import { UpdateDetailsService } from "./update-details.service";

@Component({
  selector: 'app-update-details',
  templateUrl: './update-details.component.html',
  styleUrls: ['./update-details.component.css']
})
export class UpdateDetailsComponent implements OnInit {
  protected formdata;  
  protected errorMessage: string = "";
  protected successMessage: string = "";
  public home = true;
  public update = false;
  constructor(private router: Router, private updateDetailsService: UpdateDetailsService) { }

  ngOnInit() {
   // this.updateDetailsService.getUserinfo()

    this.formdata = new FormGroup({
      fname: new FormControl(""),
      mname: new FormControl(""),
      lname: new FormControl(""),
      cid: new FormControl(""),
      uname: new FormControl(""),
      passwd: new FormControl(""),
      contactNumber: new FormControl(""),
      email: new FormControl(""),
      address: new FormControl(""),
      upload: new FormControl("")
    });
  }

  onClickSubmit(data) {
    console.log("Inside submit button " + JSON.stringify(data));

  }

  onSignOut(){
    this.router.navigate(['login']);
  }
 
  updateDetails(){
    this.home = false;
    this.update = true;
  }

}


