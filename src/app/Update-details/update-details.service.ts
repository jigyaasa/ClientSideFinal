import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Injectable()
export class UpdateDetailsService {

    constructor(private http: Http) { }
    
    getUserinfo(id) {
        return  this.http.get("http://ec2-18-218-131-46.us-east-2.compute.amazonaws.com/getInfo/"+ id) 
            .map(res => res)
    }

}